#ifndef GRILLE_H
#define GRILLE_H

/*
 * Les cellules sont le plateau actuel et pour des simplifications de calculs
 * nous avons le nombres de salles restantes des bateaux dans un tableau
 */
typedef struct{
	//La grille '-' pour rien 'n' pour bateau n avec 1 <= n <= 5
	char cellule[10][10];

	//La vie des bateaux
	int bateaux[5];
}Grille;

#endif /* GRILLE_H */ 

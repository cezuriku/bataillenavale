#ifndef JOUEUR_H
#define JOUEUR_H
#include <stdint.h>

typedef struct {
    char nom[20];
    uint16_t port;
    uint32_t ip;
    uint8_t pret;
} Joueur;

void demanderNom(char nom[20]);
void enregistrerJoueurs(uint8_t n, char *msg, Joueur * joueurs);
void afficherJoueurs(uint8_t n, Joueur * joueurs);
void envoyerJoueurs(uint8_t n, Joueur * joueurs);

#endif              /* JOUEUR_H */

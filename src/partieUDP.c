#include <sys/types.h>
#include <sys/socket.h>

#include <arpa/inet.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "partieUDP.h"

int envoiMessageHello(char *ip, char *port)
{
	int soc;
	struct sockaddr_in serveur;
	socklen_t longueur_sockaddr = sizeof(serveur);

	char message[26];
	
	serveur.sin_family = AF_INET;
	serveur.sin_port = htons((uint16_t) atoi(port));
	serveur.sin_addr.s_addr = inet_addr(ip);

	//On enregistre le nom
	sprintf(message, "HELLO ");
	demanderNom(message + 6);

	creationSocketUDP(&soc);

	//On envoie le message de connexion
	if (sendto
	    (soc, message, 26, 0, (struct sockaddr *)&serveur,
	     longueur_sockaddr) < 0) {
		fprintf(stderr, "Erreur lors du sendto ip:%s port:%s\n", ip,
			port);
		exit(EXIT_FAILURE);
	}

	return soc;
}

void creationSocketUDP(int *soc)
{
	int optval;
	//On créé une socket
	if ((*soc = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
		perror("Erreur socket");
		exit(EXIT_FAILURE);
	}
	//On met l'option SO_REUSEADDR à VRAI (1)
	optval = 1;
	if (setsockopt(*soc, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval))
	    < 0) {
		perror("Erreur setsockopt SO_REUSEADDR");
		exit(EXIT_FAILURE);
	}
	//On met l'option IP_PKTINFO à VRAI (1)
	optval = 1;
	if (setsockopt(*soc, SOL_IP, IP_PKTINFO, &optval, sizeof(optval)) < 0) {
		perror("Erreur setsockopt IP_PKTINFO");
		exit(EXIT_FAILURE);
	}
	//On met un timeout de 60 secondes
	struct timeval tv;
	tv.tv_sec = 60;
	tv.tv_usec = 0;
	if (setsockopt(*soc, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)) < 0) {
		perror("Erreur setsockopt SO_RCVTIMEO");
	}
}

void attendreConnexion(int soc, Joueur ** joueurs, uint8_t * nbJoueurs,
		       int premierJoueur)
{
	struct sockaddr_in adrServeur;
	socklen_t longueur = sizeof(adrServeur);
	adrServeur.sin_addr.s_addr = INADDR_ANY;
	adrServeur.sin_port = 0;
	adrServeur.sin_family = AF_INET;
	char nomServeur[20];

	if (premierJoueur
	    && bind(soc, (struct sockaddr *)&adrServeur, longueur) < 0) {
		perror("Erreur bind");
		exit(EXIT_FAILURE);
	}

	if (getsockname(soc, (struct sockaddr *)&adrServeur, &longueur) < 0) {
		perror("Erreur getsockname");
		exit(EXIT_FAILURE);
	}

	char tampon[528];
	char vide = '\0';
	memcpy(tampon, &vide, 528);
	char tmp[100];
	struct msghdr msg;
	struct sockaddr_in adrJoueur;
	bzero(&msg, sizeof(msg));
	msg.msg_name = &adrJoueur;
	msg.msg_namelen = sizeof(adrJoueur);
	msg.msg_control = tmp;
	msg.msg_controllen = sizeof(tmp);
	struct iovec io;
	io.iov_base = tampon;
	io.iov_len = 528;
	msg.msg_iov = &io;
	msg.msg_iovlen = 1;

	if (premierJoueur) {
		demanderNom(nomServeur);
	}

	printf("Attente de connexion sur le port %d\n",
	       ntohs(adrServeur.sin_port));
	// On récupère le message
	ssize_t longueurMessage = recvmsg(soc, &msg, 0);

	if (premierJoueur) {
		//Si l'on est le premier joueur on attend un autre joueur
		while (longueurMessage < 0 || strncmp(tampon, "HELLO ", 6)) {
			longueurMessage = recvmsg(soc, &msg, 0);
		}
		
		afficherMessageHello(tampon, longueurMessage);

		//On enregistre l'ip du serveur
		actualiserIP(&msg, &adrServeur);

		*nbJoueurs = 2;

		//On alloue la place pour les joueurs
		*joueurs = malloc(sizeof(Joueur) * (*nbJoueurs));

		memcpy((*joueurs)[0].nom, nomServeur, 20);
		(*joueurs)[0].port = adrServeur.sin_port;
		(*joueurs)[0].ip = adrServeur.sin_addr.s_addr;
		(*joueurs)[0].pret = 0;

		memcpy((*joueurs)[1].nom, tampon + 6, 20);
		(*joueurs)[1].port = adrJoueur.sin_port;
		(*joueurs)[1].ip = adrJoueur.sin_addr.s_addr;
		(*joueurs)[1].pret = 0;

		afficherJoueurs(*nbJoueurs, *joueurs);

		envoyerJoueurs(*nbJoueurs, *joueurs);
	} else {
		//On attend la première liste des joueurs
		while (longueurMessage < 0 || strncmp(tampon, "JOUEURS", 7)) {
			longueurMessage = recvmsg(soc, &msg, 0);
		}
		
		afficherMessageJoueurs(tampon, longueurMessage);

		*nbJoueurs = (uint8_t) tampon[7];

		//On alloue la place pour les joueurs
		*joueurs = malloc(sizeof(Joueur) * (*nbJoueurs));

		enregistrerJoueurs((*nbJoueurs), tampon + 8, *joueurs);
		afficherJoueurs((*nbJoueurs), *joueurs);
	}

	/* A partir d'ici on a tous un tableau d'au moins 2 joueurs
	 * On attend juste pour voir si il y a une connexion supplémentaire
	 */
	longueurMessage = 1;
	
	while (longueurMessage > 0 && (*nbJoueurs) < 20) {
		longueurMessage = recvmsg(soc, &msg, 0);
		if (longueurMessage > 0 && !strncmp(tampon, "JOUEURS", 7)) {
			afficherMessageJoueurs(tampon, longueurMessage);
			
			*nbJoueurs = (uint8_t) tampon[7];

			//On re-alloue la place pour les joueurs
			*joueurs =
			    realloc(*joueurs, sizeof(Joueur) * (*nbJoueurs));

			enregistrerJoueurs((*nbJoueurs), tampon + 8, *joueurs);
			afficherJoueurs((*nbJoueurs), *joueurs);
		} else if (longueurMessage > 0 && !strncmp(tampon, "HELLO ", 6)) {
			afficherMessageHello(tampon, longueurMessage);

			(*nbJoueurs)++;

			//On re-alloue la place pour les joueurs
			*joueurs =
			    realloc(*joueurs, sizeof(Joueur) * (*nbJoueurs));

			memcpy((*joueurs)[*nbJoueurs - 1].nom, tampon + 6, 20);
			(*joueurs)[*nbJoueurs - 1].port = adrJoueur.sin_port;
			(*joueurs)[*nbJoueurs - 1].ip =
			    adrJoueur.sin_addr.s_addr;
			(*joueurs)[*nbJoueurs - 1].pret = 0;

			afficherJoueurs(*nbJoueurs, *joueurs);

			envoyerJoueurs(*nbJoueurs, *joueurs);
		}
	}
}

void actualiserIP(struct msghdr *msg, struct sockaddr_in *adr)
{
	struct cmsghdr *pcmsg;

	for (pcmsg = CMSG_FIRSTHDR(msg);
	     pcmsg != NULL; pcmsg = CMSG_NXTHDR(msg, pcmsg)) {
		if ((pcmsg->cmsg_level == SOL_IP) &&
		    (pcmsg->cmsg_type == IP_PKTINFO)) {
			struct in_pktinfo *i =
			    (struct in_pktinfo *)CMSG_DATA(pcmsg);
			adr->sin_addr = i->ipi_addr;
			break;
		}
	}
}

void afficherMessageHello(char *message, ssize_t longueur) {
	printf("Message : {%s}[%d]\n", message, (int) longueur);
}

void afficherMessageJoueurs(char *message, ssize_t longueur) {
	uint8_t nb = (uint8_t) message[7];
	uint8_t i;
	uint8_t *ip;
	Joueur j;
	
	printf("Message : {JOUEURS %d", nb);
	for(i = 0; i < nb; i++) {
		memcpy(&(j.ip), message + i * 26 + 8, 4);
		memcpy(&(j.port), message + 4 + i * 26 + 8, 2);
		memcpy(j.nom, message + i * 26 + 6 + 8, 20);
		ip = (uint8_t *) &(j.ip);
		printf(" \"%s\" %d.%d.%d.%d:%d",
		       j.nom, ip[0], ip[1], ip[2], ip[3],
		       ntohs(j.port));
	}
	
	printf("} [%d]\n", (int) longueur);
}

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "main.h"
#include "joueur.h"
#include "partieUDP.h"
#include "partieTCP.h"

int main(int argc, char *argv[])
{
	Joueur *joueurs;
	uint8_t nbJoueurs;

	int socUDP;
	int *socTCP;
	uint8_t i;

	if (argc == 1) {
		/* Ici on a rien demandé donc on attend de recevoir un premier
		 * message hello et l'on enverra le tableau de joueurs
		 */
		creationSocketUDP(&socUDP);
		attendreConnexion(socUDP, &joueurs, &nbJoueurs, 1);
	} else if (argc == 2) {
		/* Ici on a demandé de se connecter sur un certain port 
		 * l'adresse ip sera donc 127.0.0.1
		 * on envoie donc le message hello et l'on attend le tableau de joueurs
		 */
		char ip[] = "127.0.0.1";
		socUDP = envoiMessageHello(ip, argv[1]);
		attendreConnexion(socUDP, &joueurs, &nbJoueurs, 0);
	} else if (argc == 3) {
		/* Ici on a demandé de se connecter à une certaine adresse ip
		 * on envoie donc le message hello et l'on attend le tableau de joueurs
		 */
		socUDP = envoiMessageHello(argv[1], argv[2]);
		attendreConnexion(socUDP, &joueurs, &nbJoueurs, 0);
	} else {
		/* Si l'on a passé un nombe de paramètre différent de 0 ou 2
		 * On explique comment la commande s'utilise
		 */
		printf("Usage : %s [<ip> <port>]\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	close(socUDP);

	/*
	 * La socket n°0 correspond à la socket qui va écouter et accepter les
	 * connexions TCP les autres sockets seront des sockets TCP connéctés
	 * aux joueurs 1 à n le joueur 0 étant nous même
	 */
	socTCP = malloc(sizeof(int) * nbJoueurs);

	creationSocketTCP(socTCP);
	connexionTCP(socTCP, joueurs, nbJoueurs);

	for (i = 1; i < nbJoueurs; i++)
		fermerSocket(socTCP + i);

	free(socTCP);
	free(joueurs);
	return 0;
}

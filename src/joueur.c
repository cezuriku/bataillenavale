#include <arpa/inet.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "joueur.h"

void demanderNom(char nom[20])
{
	char *ligne = NULL;

	//On enregistre le nom
	printf("Quel est votre nom?\n");
	size_t nomLongueurMax = 0;
	size_t nomLongueur = (size_t) getline(&ligne, &nomLongueurMax, stdin);

	//On retire le dernier caractère '\n' ou l'on reduit a 19 la taille du nom
	if (nomLongueur < 20)
		nomLongueur--;
	else
		nomLongueur = 19;
	nomLongueurMax = 20;

	//On fait du bourrage
	size_t i;
	for (i = 0; i < 20; i++) {
		if (i < nomLongueur)
			nom[i] = ligne[i];
		else
			nom[i] = '\0';
	}

	free(ligne);
}

void afficherJoueurs(uint8_t n, Joueur * joueurs)
{
	uint8_t i;
	uint8_t *ip;

	for (i = 0; i < n; i++) {
		ip = (uint8_t *) & (joueurs[i].ip);
		printf("Joueur %d : \"%s\" %d.%d.%d.%d: %d\n", i,
		       joueurs[i].nom, ip[0], ip[1], ip[2], ip[3],
		       ntohs(joueurs[i].port));
	}
}

void envoyerJoueurs(uint8_t n, Joueur * joueurs)
{
	uint8_t i, j, k;
	char tampon[528];
	strcpy(tampon, "JOUEURS");
	tampon[7] = (char) n;
	int soc;
	struct sockaddr_in serveur;
	socklen_t longueur_sockaddr = sizeof(serveur);

	if ((soc = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
		perror("Erreur socket");
	}
	//On envoie a tout les joueurs sauf le joueur 0 qui représente notre joueur
	for (i = 1; i < n; i++) {
		for (j = 0; j < n; j++) {
			k = (uint8_t) (j + i) % n;
			memcpy(tampon + 8 + j * 26, (char *)&(joueurs[k].ip),
			       4);
			memcpy(tampon + 12 + j * 26,
			       (char *)&(joueurs[k].port), 2);
			memcpy(tampon + 14 + j * 26, joueurs[k].nom, 20);
		}

		serveur.sin_family = AF_INET;
		serveur.sin_port = joueurs[i].port;
		serveur.sin_addr.s_addr = joueurs[i].ip;
		if (sendto
		    (soc, tampon, (size_t) (8 + n * 26), 0, (struct sockaddr *)&serveur,
		     longueur_sockaddr) < 0)
			perror("sendto error");
	}

	close(soc);
}

void enregistrerJoueurs(uint8_t n, char *msg, Joueur * joueurs)
{
	uint8_t i;

	for (i = 0; i < n; i++) {
		memcpy(&(joueurs[i].ip), msg + i * 26, 4);

		memcpy(&(joueurs[i].port), msg + 4 + i * 26, 2);

		memcpy(joueurs[i].nom, msg + i * 26 + 6, 20);
	}
}

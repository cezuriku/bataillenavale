#include <sys/types.h>
#include <sys/socket.h>
#include <linux/tcp.h>

#include <arpa/inet.h>

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "partieTCP.h"

void creationSocketTCP(int *soc)
{
	int optval;
	socklen_t optlen = sizeof(optval);

	//On créé une socket
	if ((*soc = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
		perror("Erreur socket");
		exit(EXIT_FAILURE);
	}
	//On met l'option SO_REUSEADDR à VRAI (1)
	optval = 1;
	if (setsockopt(*soc, SOL_SOCKET, SO_REUSEPORT, &optval, sizeof(optval))
	    < 0) {
		perror("Erreur setsockopt SO_REUSEPORT");
		exit(EXIT_FAILURE);
	}
	//On met l'option KEEPALIVE a VRAI
	optval = 1;
	if (setsockopt(*soc, SOL_SOCKET, SO_KEEPALIVE, &optval, optlen) < 0) {
		perror("Erreur setsockopt TCP_KEEPALIVE");
	}
	//On met l'option KEEPIDLE a 15
	optval = 15;
	if (setsockopt(*soc, IPPROTO_TCP, TCP_KEEPIDLE, &optval, optlen) < 0) {
		perror("Erreur setsockopt TCP_KEEPIDLE");
	}
	//On met l'option KEEPINTVL a 3
	optval = 3;
	if (setsockopt(*soc, IPPROTO_TCP, TCP_KEEPINTVL, &optval, optlen) < 0) {
		perror("Erreur setsockopt TCP_KEEPINTVL");
	}
	//On met un timeout de 60 secondes
	struct timeval tv;
	tv.tv_sec = 60;
	tv.tv_usec = 0;
	if (setsockopt(*soc, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)) < 0) {
		perror("Erreur setsockopt SO_RCVTIMEO");
	}
}

void connexionTCP(int *sockets, Joueur * joueurs, uint8_t nbJoueurs)
{
	struct sockaddr_in serveur;
	struct sockaddr_in client;
	char tampon[4];
	uint8_t i, k, connexionOK;
	uint8_t nbJoueursRestants = 0;
	for (i = 1; i < nbJoueurs; i++) {
		sockets[i] = -1;
	}
	socklen_t longueurSockaddr = sizeof(struct sockaddr_in);

	serveur.sin_family = AF_INET;
	serveur.sin_port = htons(joueurs[0].port);
	serveur.sin_addr.s_addr = INADDR_ANY;

	if (bind
	    (*sockets, (struct sockaddr *)&serveur,
	     sizeof(struct sockaddr_in)) < 0)
		perror("Erreur bind");

	if ((listen(*sockets, 0)) < 0)
		perror("Erreur listen");

	int sockTmp;
	for (i = 1; i < nbJoueurs; i++) {
		if ((htons(joueurs[i].port) < htons(joueurs[0].port)) ||
		    (htons(joueurs[i].port) == htons(joueurs[0].port) &&
		     (htons(joueurs[i].ip) < htons(joueurs[0].ip)))) {
			if ((sockTmp =
			     accept(*sockets, (struct sockaddr *)&serveur,
				    &longueurSockaddr)) < 0)
				perror("Erreur accept");
			else {

				//On recherche a qui appartient la socket
				for (k = 1; k < nbJoueurs; k++) {
					if (serveur.sin_addr.s_addr ==
					    joueurs[k].ip
					    && serveur.sin_port ==
					    htons(joueurs[k].port)) {
						sockets[k] = sockTmp;
						printf
						    ("Connexion acceptée de %s[%d]\n",
						     joueurs[k].nom, k);
						nbJoueursRestants++;
					}
				}
			}
		}
	}

	//On ferme le listener
	fermerSocket(sockets);

	//Maintenant nous sommes client
	client.sin_family = AF_INET;
	client.sin_port = htons(joueurs[0].port);
	client.sin_addr.s_addr = INADDR_ANY;

	int j;
	for (i = 1; i < nbJoueurs; i++) {
		if ((htons(joueurs[i].port) > htons(joueurs[0].port)) ||
		    (htons(joueurs[i].port) == htons(joueurs[0].port) &&
		     (htons(joueurs[i].ip) > htons(joueurs[0].ip)))) {

			creationSocketTCP(&(sockets[i]));

			if (bind
			    (sockets[i], (struct sockaddr *)&client,
			     sizeof(struct sockaddr_in)) < 0)
				perror("Erreur bind");

			serveur.sin_family = AF_INET;
			serveur.sin_port = htons(joueurs[i].port);
			serveur.sin_addr.s_addr = joueurs[i].ip;

			j = 1;
			connexionOK = 0;
			/* On tente de se connecter 5 fois avec un intervalle de 5 secondes
			 * On fait juste ça au cas où le serveur n'as pas encore 
			 * fait son accept
			 */
			while (j <= 5 && !connexionOK) {
				if (connect
				    (sockets[i], (struct sockaddr *)&serveur,
				     sizeof(serveur)) < 0) {
					fprintf(stderr,
						"Connexion échouée sur %s[%d] Tentative (%d / 5) : %s\n",
						joueurs[i].nom, i, j,
						strerror(errno));
					if (j != 5)
						sleep(5);
					else {
						fermerSocket(sockets + i);
					}
				} else {
					printf("Connexion reussie sur %s[%d]\n",
					       joueurs[i].nom, i);
					connexionOK = 1;
					nbJoueursRestants++;
				}
				j++;
			}
		}
	}

	//On envoie PRET a chaque joueur
	for (i = 1; i < nbJoueurs; i++) {
		if (sockets[i] != -1) {

			/* On met un timeout de [60 * (nbJoueursPerdus + 1)] secondes
			 * Pour permettre a ceux qui vont etre bloqué pendant 60 secondes
			 * de pouvoir attendre leur message tout de meme
			 */
			struct timeval tv;
			tv.tv_sec = 60 * (nbJoueurs - nbJoueursRestants + 1);
			tv.tv_usec = 0;
			if (setsockopt
			    (sockets[i], SOL_SOCKET, SO_RCVTIMEO, &tv,
			     sizeof(tv)) < 0) {
				perror("Erreur setsockopt SO_RCVTIMEO");
			}

			if (write(sockets[i], "PRET", 4) < 0) {
				perror("Erreur write");
				fermerSocket(sockets + i);
			}
		}
	}

	//Nous sommes pret
	joueurs[0].pret = 1;

	//On reçois pret de chaque joueur
	for (i = 1; i < nbJoueurs; i++) {
		if (sockets[i] != -1) {
			if (read(sockets[i], tampon, 4) < 0)
				perror("Erreur read");
			if (!strncmp("PRET", tampon, 4))
				(joueurs[i]).pret = 1;
		} else
			fermerSocket(sockets + i);
	}

	//On affiche si les joueurs sont pret
	for (i = 0; i < nbJoueurs; i++) {
		printf("Joueur n°%u \"%s\", pret = %d\n", i, joueurs[i].nom,
		       joueurs[i].pret);
	}
}

void fermerSocket(int *socket)
{
	if (*socket != -1)
		if (close(*socket) < 0)
			perror("Erreur close");

	*socket = -1;
}

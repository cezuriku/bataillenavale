#ifndef PARTIE_TCP_H
#define PARTIE_TCP_H
#include "joueur.h"

void creationSocketTCP(int *soc);
void connexionTCP(int * sockets, Joueur * joueurs, uint8_t nbJoueurs);
void fermerSocket(int * socket);

#endif				/* PARTIE_TCP_H */

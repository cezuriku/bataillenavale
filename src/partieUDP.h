#ifndef PARTIE_UDP_H
#define PARTIE_UDP_H
#include <netinet/in.h>

#include "joueur.h"

int envoiMessageHello(char *ip, char *port);
void afficherMessageHello(char *message, ssize_t longueur);
void afficherMessageJoueurs(char *message, ssize_t longueur);
void creationSocketUDP(int *soc);
void attendreConnexion(int soc, Joueur ** joueurs, uint8_t * nbJoueurs,
			       int premierJoueur);
void actualiserIP(struct msghdr *msg, struct sockaddr_in *adr);

#endif				/* PARTIE_UDP_H */
